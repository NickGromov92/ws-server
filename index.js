const http = require('http');
const ws = require('websocket').server;
const { v4: uuidv4 } = require('uuid');
const PORT = 8081;

let connections = {};

const httpServer = http.createServer(function(request, response) {
	console.log(`${new Date()} - Request from ${request.url}`);
	response.writeHead(404);
	response.end();
});

const wsServer = new ws({
	httpServer,
	autoAcceptConnections: false
});

function checkOrigin (originUrl) {
	return 'http://localhost:63342' === originUrl;
}

wsServer.on('request', (request) => {
	if(!checkOrigin(request.origin)) {
		request.reject();
		console.log(`${new Date()} - ${request.origin} was rejected`);
	}

	const connection = request.accept('echo-protocol', request.origin);
	let clientId = uuidv4();
	console.log(`${new Date()} - WS connection accepted`);

	connection.on('message', (message) => {
		if(message.type === 'utf8') {

			let parseMessage = JSON.parse(message.utf8Data);

			if(parseMessage.type === 'name') {
				connections[clientId] = {
					connection,
					name: parseMessage.name
				}

				let messageObj = {
					type: 'id',
					id: clientId
				}

				connection.send(JSON.stringify(messageObj));
			}

			if(parseMessage.type === 'read') {
				Object.values(connections).forEach(client => {
					let messageObj = {
						type: 'read',
						id: parseMessage.id,
					}
					client.connection.send(JSON.stringify(messageObj));
				});
			}

			if(parseMessage.type === 'message') {

				let date = new Date();
				let options = {
					hour: 'numeric',
					minute: 'numeric',
				};

				date = date.toLocaleString("en-US", options);

				let messageId = uuidv4();
				let messageObj = {
					type: 'message',
					date,
					message: parseMessage.message,
					name: parseMessage.name,
					isRead: false,
					id: messageId,
				}

				Object.values(connections).forEach(client => {
					messageObj.own = client.connection === connection;
					client.connection.send(JSON.stringify(messageObj));
				})
			}

		}
	});

	connection.on('close', (closeCode, closeText) => {
		console.log(`${new Date()} - Connection closed ${closeCode} ${closeText}`);
		delete connections[clientId];
	});
});

httpServer.listen(PORT, () => {
	console.log(`${new Date()} - Server is listening on port ${PORT}`)
});
