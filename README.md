# ws-server

Node.js WS server for learning

## Getting Started

These instructions will get you a copy of the project up and run on your local machine. For the default, the server runs at 8081 port.

### Prerequisites

You need to **node.js** and **npm** for run project on your local machine - https://nodejs.org/

### Project setup
```
npm install
```

### Run server
```
node index.js
```

## Authors

* **Nick Gromov** - *Initial work* - [NickGromov92](https://gitlab.com/NickGromov92)
